//
//  rhimportd
//
//  The Radio Helsinki Rivendell Import Daemon
//
//
//  Copyright (C) 2015-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhimportd.
//
//  rhimportd is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhimportd is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhimportd. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
	"strings"

	"code.helsinki.at/rhrd-go/rddb"
	"code.helsinki.at/rhrd-go/rhimport"
	"github.com/spreadspace/telgo"
)

func telnetQuit(c *telgo.Client, args []string, conf *rhimport.Config, db *rddb.DB, sessions *rhimport.SessionStore) bool {
	return true
}

func telnetHelp(c *telgo.Client, args []string, conf *rhimport.Config, db *rddb.DB, sessions *rhimport.SessionStore) bool {
	switch len(args) {
	case 2:
		switch args[1] {
		case "quit":
			c.Sayln("usage: quit")
			c.Sayln("   terminates the client connection. You may also use Ctrl-D to do this.")
			return false
		case "help":
			c.Sayln("usage: help [ <cmd> ]")
			c.Sayln("   prints command overview or detailed info to <cmd>.")
			return false
		case "set":
			c.Sayln("usage: set <param> <value>")
			c.Sayln("   this sets the import parameter <param> to <value>.")
			c.Sayln("")
			c.Sayln("  available parameters:")
			c.Sayln("    UserName             string   username to use for rdxport interface")
			c.Sayln("    Password             string   password to use for rdxport interface")
			c.Sayln("    SourceUri            string   uri to the file to import")
			c.Sayln("    SourceFilePolicy     enum     one of: keep, delete")
			c.Sayln("    FetchConverter       string   fetch converter to use")
			c.Sayln("                                  (one of: null, ffmpeg, bs1770, ffmpeg-bs1770)")
			c.Sayln("    ShowId               uint     the RHRD show id to import to")
			c.Sayln("    ClearShowCarts       bool     clear all show-carts before importing?")
			c.Sayln("    GroupName            string   name of music-pool group to import to")
			c.Sayln("    Cart                 uint     cart to import to")
			c.Sayln("    ClearCart            bool     remove/add cart before import")
			c.Sayln("    Cut                  uint     cut to import to")
			c.Sayln("    Channels             uint     number of audio channels (default: %v)", conf.ImportParamDefaults.Channels)
			c.Sayln("    NormalizationLevel   int      normalization level in dB (default: %v)", conf.ImportParamDefaults.NormalizationLevel)
			c.Sayln("    AutotrimLevel        int      autotrim level in dB (default: %v)", conf.ImportParamDefaults.AutotrimLevel)
			c.Sayln("    UseMetaData          bool     extract meta data from file (default: %v)", conf.ImportParamDefaults.UseMetaData)
			c.Sayln("")
			c.Sayln("  UserName, Password and SourceUri are mandatory parameters.")
			c.Sayln("")
			c.Sayln("  If ShowId is supplied, GroupName, Channels, NomalizationLevel, AutorimLevel,")
			c.Sayln("  UseMetaData and Cut will be ignored. The values from the shows' dropbox will")
			c.Sayln("  be used instead. Cart may be specified but must point to a cart within that")
			c.Sayln("  show which must be either empty or ClearCart must be set to true. The latter")
			c.Sayln("  tells rhimportd to delete and recreate the cart before importing. If Cart is")
			c.Sayln("  set to 0 the next free cart in the show will be used. Show carts will always")
			c.Sayln("  be imported into cut 1.")
			c.Sayln("")
			c.Sayln("  If GroupName is supplied but ShowId is not, Channels, NomalizationLevel,")
			c.Sayln("  AutorimLevel, UseMetaData, Cut, Cart and ClearCart will be ignored. The")
			c.Sayln("  values from the music pools' dropbox will be used instead. The file will")
			c.Sayln("  always be imported into cut 1 of the first free cart within the music pool.")
			c.Sayln("")
			c.Sayln("  If ShowId and GroupName are omitted a Cart must be specified. Cut may be")
			c.Sayln("  supplied in which case both cart and cut must already exist. The import will")
			c.Sayln("  then replace the contents of the current data stored in Cart/Cut. If only")
			c.Sayln("  Cart and no Cut is supplied and ClearCart is false the file will either get")
			c.Sayln("  imported into the next free cut, in case the cart exists, or the cart will")
			c.Sayln("  be created and the file will be imported into cut 1 of this cart.")
			c.Sayln("  If ClearCart is true the cart will be deleted, recreated and the file will")
			c.Sayln("  be imported into cut 1 of this cart.")
			c.Sayln("")
			c.Sayln("  In case of an error carts and cuts which might got created will be removed.")
			c.Sayln("  Carts which got deleted because of ClearShowCarts or ClearCart are however")
			c.Sayln("  gone for good.")
			c.Sayln("")
			c.Sayln("  The SourceFilePolicy specifies how to deal with temporary files which get")
			c.Sayln("  created during the import process.")
			c.Sayln("  The policy 'delete', which is the default, deletes all temporary files")
			c.Sayln("  regardless of whether the import was a success or not.")
			c.Sayln("  The 'keep' mode tells rhimportd to leave temporary files after successfull")
			c.Sayln("  imports. In this case the result will contain a tmp:// fetch-uri which can")
			c.Sayln("  be used to re-import the file to a different show, pool or jingle.")
			c.Sayln("  Mind that tmp:// fetch-uris will bypass the fetch-converter as well as the")
			c.Sayln("  normalizer and just import the file as it is. This is because files inside")
			c.Sayln("  tmp:// have already been processed before. Also mind that even in 'keep'")
			c.Sayln("  mode files might get deleted if the fetching or normalizing fails. Only the")
			c.Sayln("  last step, the actual import, may fail.")
			c.Sayln("  ")
			return false
		case "show":
			c.Sayln("usage: show")
			c.Sayln("   this prints the current values of all import parameters.")
			return false
		case "reset":
			c.Sayln("usage: reset")
			c.Sayln("   this resets all import parameters to default values.")
			return false
		case "run":
			c.Sayln("usage: run")
			c.Sayln("   this starts the fetch/import process according to the current")
			c.Sayln("   import parameters.")
			return false
		}
		fallthrough
	default:
		c.Sayln("usage: <cmd> [ [ <arg1> ] ... ]")
		c.Sayln("  available commands:")
		c.Sayln("    quit                   close connection (or use Ctrl-D)")
		c.Sayln("    help [ <cmd> ]         print this, or help for specific command")
		c.Sayln("    set <param> <value>    sets parameter <param> on current import context")
		c.Sayln("    show                   shows current import context")
		c.Sayln("    reset                  resets current import context")
		c.Sayln("    run                    runs fetch/import using current import context")
	}
	return false
}

func telnetSetInt(c *telgo.Client, param *int, val string) {
	if vint, err := strconv.ParseInt(val, 10, 32); err != nil {
		c.Sayln("invalid value (must be an integer)")
	} else {
		*param = int(vint)
	}
}

func telnetSetUint(c *telgo.Client, param *uint, val string) {
	if vuint, err := strconv.ParseUint(val, 10, 32); err != nil {
		c.Sayln("invalid value (must be a positive integer)")
	} else {
		*param = uint(vuint)
	}
}

func telnetSetBool(c *telgo.Client, param *bool, val string) {
	if vbool, err := strconv.ParseBool(val); err != nil {
		c.Sayln("invalid value (must be true or false)")
	} else {
		*param = vbool
	}
}

func telnetSetFilePolicy(c *telgo.Client, param *rhimport.FilePolicy, val string) {
	if err := param.FromString(val); err != nil {
		c.Sayln("invalid value (%s)", err)
	}
}

func telnetSet(c *telgo.Client, args []string, conf *rhimport.Config, db *rddb.DB, sessions *rhimport.SessionStore) bool {
	if len(args) != 3 {
		c.Sayln("wrong number of arguments")
		return false
	}

	var ctx *rhimport.Context
	if c.UserData == nil {
		c.UserData = rhimport.NewContext(conf, nil, getStdLog("sess-%s-std"), getDbgLog("sess-%s-dbg"))
		ctx = c.UserData.(*rhimport.Context)
		ctx.Trusted = false
	} else {
		ctx = c.UserData.(*rhimport.Context)
	}
	switch strings.ToLower(args[1]) {
	case "username":
		ctx.UserName = args[2]
	case "password":
		ctx.Password = args[2]
	case "sourceuri":
		ctx.SourceUri = args[2]
	case "sourcefilepolicy":
		telnetSetFilePolicy(c, &ctx.SourceFilePolicy, args[2])
	case "fetchconverter":
		ctx.FetchConverter = args[2]
	case "showid":
		telnetSetUint(c, &ctx.ShowId, args[2])
	case "clearshowcarts":
		telnetSetBool(c, &ctx.ClearShowCarts, args[2])
	case "groupname":
		ctx.GroupName = args[2]
	case "cart":
		telnetSetUint(c, &ctx.Cart, args[2])
	case "clearcart":
		telnetSetBool(c, &ctx.ClearCart, args[2])
	case "cut":
		telnetSetUint(c, &ctx.Cut, args[2])
	case "channels":
		telnetSetUint(c, &ctx.Channels, args[2])
	case "normalizationlevel":
		telnetSetInt(c, &ctx.NormalizationLevel, args[2])
	case "autotrimlevel":
		telnetSetInt(c, &ctx.AutotrimLevel, args[2])
	case "usemetadata":
		telnetSetBool(c, &ctx.UseMetaData, args[2])
	default:
		c.Sayln("unknown parameter, use 'help set' for a list of available parameters")
	}
	return false
}

func telnetReset(c *telgo.Client, args []string, conf *rhimport.Config, db *rddb.DB, sessions *rhimport.SessionStore) bool {
	if len(args) > 1 {
		c.Sayln("too many arguments")
		return false
	}

	c.UserData = nil
	return false
}

func telnetShow(c *telgo.Client, args []string, conf *rhimport.Config, db *rddb.DB, sessions *rhimport.SessionStore) bool {
	if len(args) > 1 {
		c.Sayln("too many arguments")
		return false
	}

	if c.UserData != nil {
		ctx := c.UserData.(*rhimport.Context)
		c.Sayln(" UserName: %q", ctx.UserName)
		c.Sayln(" Password: %q", ctx.Password)
		c.Sayln(" SourceUri: %q", ctx.SourceUri)
		c.Sayln(" SourceFilePolicy: %s", ctx.SourceFilePolicy.String())
		c.Sayln(" FetchConverter: %q", ctx.FetchConverter)
		c.Sayln(" ShowId: %v", ctx.ShowId)
		c.Sayln(" ClearShowCarts: %v", ctx.ClearShowCarts)
		c.Sayln(" GroupName: %q", ctx.GroupName)
		c.Sayln(" Cart: %v", ctx.Cart)
		c.Sayln(" ClearCart: %v", ctx.ClearCart)
		c.Sayln(" Cut: %v", ctx.Cut)
		c.Sayln(" Channels: %v", ctx.Channels)
		c.Sayln(" NormalizationLevel: %v", ctx.NormalizationLevel)
		c.Sayln(" AutotrimLevel: %v", ctx.AutotrimLevel)
		c.Sayln(" UseMetaData: %v", ctx.UseMetaData)
	} else {
		c.Sayln("context is empty")
	}
	return false
}

func telnetProgressCallback(step int, stepName string, current, total float64, title string, cart, cut uint, userdata interface{}) bool {
	if math.IsNaN(current) || math.IsInf(current, 0) {
		current = 0.0
	}
	if math.IsNaN(total) || math.IsInf(total, 0) {
		total = 0.0
	}
	c := userdata.(chan<- rhimport.ProgressData)
	select {
	case c <- rhimport.ProgressData{Step: step, StepName: stepName, Current: current, Total: total, Title: title, Cart: cart, Cut: cut}:
	default:
	}
	return true
}

func telnetDoneCallback(res rhimport.Result, userdata interface{}) bool {
	c := userdata.(chan<- rhimport.Result)
	c <- res
	return true
}

func telnetRun(c *telgo.Client, args []string, conf *rhimport.Config, db *rddb.DB, sessions *rhimport.SessionStore) bool {
	if c.UserData == nil {
		c.Sayln("context is empty please set at least one option")
		return false
	}
	ctx := c.UserData.(*rhimport.Context)
	c.Sayln("starting import from '%s'", ctx.SourceUri)

	id, s, code, errstring := sessions.New(ctx, "")
	if code != http.StatusOK {
		c.Sayln("creating session failed: %s", errstring)
		return false
	}
	c.Sayln(" * got session id: %s", id)

	donechan := make(chan rhimport.Result, 1)
	if err := s.AddDoneHandler((chan<- rhimport.Result)(donechan), telnetDoneCallback); err != nil {
		c.Sayln("installing done callback failed: %s", errstring)
		return false
	}

	progresschan := make(chan rhimport.ProgressData, 10)
	if err := s.AddProgressHandler((chan<- rhimport.ProgressData)(progresschan), telnetProgressCallback); err != nil {
		c.Sayln("installing progress callback failed: %s", errstring)
		return false
	}

	s.Run(0)

	currentStep := 0
	for {
		select {
		case <-c.Cancel:
			s.Cancel()
		case p := <-progresschan:
			if currentStep != p.Step {
				if currentStep > 0 {
					c.Sayln("")
				}
				currentStep = p.Step
			}

			if p.Cart > 0 {
				c.Say("\r * %s: '%s':\t%6.2f%% (Cart/Cut: %d/%d)", p.StepName, p.Title, (p.Current/p.Total)*100, p.Cart, p.Cut)
			} else {
				c.Say("\r * %s '%s':\t%6.2f%%", p.StepName, p.Title, (p.Current/p.Total)*100)
			}
		case res := <-donechan:
			if currentStep > 0 {
				c.Sayln("")
			}
			if res.ResponseCode == http.StatusOK {
				c.Sayln("File got succesfully imported into Cart/Cut %d/%d, Source: '%s'", res.Cart, res.Cut, res.SourceFile)
			} else {
				c.Sayln("Fileimport has failed (Cart/Cut %d/%d, Source: '%s'): %s", res.Cart, res.Cut, res.SourceFile, res.ErrorString)
			}
			return false
		}
	}
}

func StartControlTelnet(addr string, conf *rhimport.Config, db *rddb.DB, sessions *rhimport.SessionStore) {
	cmdlist := make(telgo.CmdList)
	cmdlist["quit"] = func(c *telgo.Client, args []string) bool { return telnetQuit(c, args, conf, db, sessions) }
	cmdlist["help"] = func(c *telgo.Client, args []string) bool { return telnetHelp(c, args, conf, db, sessions) }
	cmdlist["set"] = func(c *telgo.Client, args []string) bool { return telnetSet(c, args, conf, db, sessions) }
	cmdlist["reset"] = func(c *telgo.Client, args []string) bool { return telnetReset(c, args, conf, db, sessions) }
	cmdlist["show"] = func(c *telgo.Client, args []string) bool { return telnetShow(c, args, conf, db, sessions) }
	cmdlist["run"] = func(c *telgo.Client, args []string) bool { return telnetRun(c, args, conf, db, sessions) }

	rhl.Println("telnet-ctrl: listening on", addr)
	s, err := telgo.NewServer(addr, "rhimportd> ", cmdlist, nil)
	if err != nil {
		fmt.Printf("telnet server returned: %s", err)
	}
	if err := s.Run(); err != nil {
		fmt.Printf("telnet server returned: %s", err)
	}
}
