//
//  rhimportd
//
//  The Radio Helsinki Rivendell Import Daemon
//
//
//  Copyright (C) 2015-2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhimportd.
//
//  rhimportd is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhimportd is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhimportd. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"net/http"
	_ "net/http/pprof"
	"time"

	"code.helsinki.at/rhrd-go/rddb"
	"code.helsinki.at/rhrd-go/rhimport"
)

type webHandler struct {
	*rhimport.Config
	*rddb.DB
	*rhimport.SessionStore
	trusted bool
	H       func(*rhimport.Config, *rddb.DB, *rhimport.SessionStore, bool, http.ResponseWriter, *http.Request)
}

func (self webHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	self.H(self.Config, self.DB, self.SessionStore, self.trusted, w, r)
}

func StartWebRouter(addr, staticDir string, conf *rhimport.Config, db *rddb.DB, sessions *rhimport.SessionStore) {
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir(staticDir))))

	//	http.Handle("/trusted/simple", webHandler{conf, db, sessions, true, webSimpleHandler})
	http.Handle("/public/simple", webHandler{conf, db, sessions, false, webSimpleHandler})
	http.Handle("/public/socket", webHandler{conf, db, sessions, false, webSocketHandler})
	http.Handle("/public/upload/", http.StripPrefix("/public/upload/", webHandler{conf, db, sessions, false, webUploadHandler}))

	rhl.Println("web-router: listening on", addr)
	server := &http.Server{Addr: addr, ReadTimeout: 12 * time.Hour, WriteTimeout: 12 * time.Hour}
	server.ListenAndServe()
}
