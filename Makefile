##
##  rhimportd
##
##  The Radio Helsinki Rivendell Import Daemon
##
##
##  Copyright (C) 2015-2016 Christian Pointner <equinox@helsinki.at>
##
##  This file is part of rhimportd.
##
##  rhimportd is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  rhimportd is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with rhimportd. If not, see <http://www.gnu.org/licenses/>.
##

curdir:= $(shell pwd)
GOCMD := GOPATH=$(curdir) go

EXECUTEABLE := rhimportd

LIBS := "code.helsinki.at/rhrd-go/rddb" \
        "code.helsinki.at/rhrd-go/rhimport" \
        "github.com/spreadspace/telgo" \
        "github.com/gorilla/websocket"


.PHONY: getlibs updatelibs vet format build clean distclean
all: build


getlibs:
	@$(foreach lib,$(LIBS), echo "fetching lib: $(lib)"; $(GOCMD) get $(lib);)

updatelibs:
	@$(foreach lib,$(LIBS), echo "updating lib: $(lib)"; $(GOCMD) get -u $(lib);)

vet:
	@echo "vetting: $(EXECUTEABLE)"
	@$(GOCMD) vet $(EXECUTEABLE)

format:
	@echo "formating: $(EXECUTEABLE)"
	@$(GOCMD) fmt $(EXECUTEABLE)

build: getlibs
	@echo "installing: $(EXECUTEABLE)"
	@$(GOCMD) install $(EXECUTEABLE)


release:
ifndef VER
	$(error "you have to set the version: VER=?")
endif
	@echo "will make release for version: $(VER)"
	@echo " - release branch"
	@git co -b rel-$(VER)
	@echo " - release tag"
	@git tag -a -m "Release $(VER)" $(VER)
	@git co master
	@echo " - debian changelog"
	@DEBEMAIL="equinox@helsinki.at" dch -v $(VER)-1
	@DEBEMAIL="equinox@helsinki.at" dch --distribution unstable --release
	@git commit -a -m "updated debian changelog after relase"
	@echo "\ndone."
	@echo "if everything went throught please run: git push --all && git push --tags"

clean:
	rm -rf pkg/*/$(EXECUTEABLE)
	rm -rf bin

distclean: clean
	@$(foreach dir,$(shell ls src/),$(if $(subst $(EXECUTEABLE),,$(dir)),$(shell rm -rf src/$(dir))))
	rm -rf pkg
