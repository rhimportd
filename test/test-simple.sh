#!/bin/sh

if [ -z "$1" ]; then
  echo "Usage: $0 <json-file>"
  exit 1
fi

#curl -XPOST 'http://localhost:4080/public/simple' -d @$1
curl -H "X-Forwarded-User: heslinki" -XPOST 'http://localhost:4080/public/simple' -d @$1

exit 0
